__author__ = 'Peter Hayes UCL CSML'

"""
Module containing mathematical functions that are used throughout the solution
"""

import theano.tensor as T

def mse(output, target):
    """
    Cost function for continuous data available as a parameter for the network class.
    """
    return T.mean((output - target) ** 2)

def nll_binary(output, target):
    """
    Cost function for categorical data available as a parameter for the network class.
    """
    return T.mean(T.nnet.binary_crossentropy(output, target))

def cat_cross(output, target):
    return -T.mean(target * T.log(output)+ (1.- target) * T.log(1. - output))

def nll_multiclass(output, target):
    """
    :param output: categorical results from model
    :param target: target results
    :return:
    ref: http://christianherta.de/lehre/dataScience/machineLearning/neuralNetworks/LSTM.php
    """
    return -T.mean(T.log(output)[T.arange(target.shape[0]), target])

def hard_sigmoid(x):
    return T.nnet.hard_sigmoid(x)

def cosine_similarity(k, M):
    """
    computes cosine similarity in terms of theano variables
    :param k: key vector
    :param M: Memory matrix
    :return:
    ref: https://github.com/shawntan/neural-turing-machines/blob/master/model.py
    """
    k_unit = k / (T.sqrt(T.sum(k**2)) + 1e-5) # add constant to help ward of numerical instability
    k_unit = k_unit.dimshuffle(('x', 0))
    M_lengths = T.sqrt(T.sum(M**2, axis=1)).dimshuffle((0, 'x'))
    M_unit = M / (M_lengths + 1e-5) # add constant to help ward of numerical instability

    return T.sum(k_unit * M_unit, axis=1)

def shift_convolve(weight, shift, shift_conv):
    """
    :param weight: shifting weight
    :param shift:
    :param shift_conv: circular convolution
    :return:
    ref:https://github.com/shawntan/theano_toolkit/blob/master/utils.py
    """
    shift = shift.dimshuffle((0, 'x'))
    return T.sum(shift * weight[shift_conv], axis=0)

def vector_softmax(vec):
    """
    :param vec: vector to compute softmax on
    :return: softmax value
    ref: https://github.com/shawntan/theano_toolkit/blob/master/utils.py
    """
    return T.nnet.softmax(vec.reshape((1, vec.shape[0])))[0]
