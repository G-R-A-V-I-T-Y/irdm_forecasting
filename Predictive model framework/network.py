__author__ = 'Peter Hayes UCL CSML'
from utils import *

"""
Module containing class definition for network, as well as seperate methods for training and testing a model object
"""

class Network:

    """
    Generic network class, with layers fully parameterised to allow for rapid dynamic network creation at train time
    """

    def __init__(self, numpy_rng,
                 n_in, n_out, loss, hid_dim=[100], hid_type=[],
                 activation = T.nnet.sigmoid,
                 inputs = None, targets = None,):

        """
        :param numpy_rng: numpy random generator
        :param n_in: number of input nodes for the network
        :param n_out: number of output nodes for the network. The output is hard coded as linear
        :param loss: loss function name used by gradient descent for the network. Function name must exist in utils.py
        :param hid_dim: Array of hidden layer dimensions, e.g. [30, 30] means 2 hidden layers of 30 nodes each.
                         the first layer takes input from the input nodes. Any subsequent layers take input from the
                         output of previous layer. The final layer will provide its output to the output layer
        :param hid_type: Array of hidden layer class type names. Must correspond to the number of dimensions provided in
                        hid_dim. e.g. [RNNLayer, RNNLayer]. Class name must exist within the collection
                        of layer classes provided in nnlayers.py module.
        :param activation: Non linear activation function to be passed to the hidden layers. Hows its used is determined
                        by the class definition for the layer.
        :param inputs: input data, num obs * n_in, representing a time series batch in this context
        :param targets: target data, num obs * n_out, representing the time lagged targets for the input data
        :return: network class instance for use with training and testing methods to follow
        """

        # Assign instance variables
        self.rng = numpy_rng
        self.input_dim = n_in
        self.hid_num = len(hid_dim)
        self.hid_dim = hid_dim
        self.hid_type = hid_type
        self.inputs = inputs # TODO add logic in case these are none
        self.targets = targets # TODO add logic in case these are none. Also consider unsupervised case in future.
        # intialise storage for hidden layers and network params
        self.hidden_layers = []
        self.params = []
        self.loss  = loss

        # define the hidden layers
        for h in range(self.hid_num):

            if h == 0: # if first layer take input from input layer
                layer_in = self.input_dim
                layer_input = self.inputs
            else: # if subsequent layer take input from previous layer
                layer_in = self.hid_dim[h-1]
                layer_input=self.hidden_layers[-1].output

            layer_class = hid_type[h]
            # create hidden layers given specification of class
            layer = layer_class(numpy_rng, layer_input, layer_in, hid_dim[h],
                                activation = activation) # remove hard coding and use hi_type input

            # network definition inherits parameters and layer definition
            self.hidden_layers.append(layer)
            self.params.extend(layer.params)

        # intialise output layer parameters
        W_ho= np.asarray(np.random.uniform(size=(hid_dim[-1], n_out),low=-.01, high=.01), dtype=fx)
        self.W_ho = th.shared(value=W_ho, name='W_ho')
        self.b   = th.shared(np.zeros(n_out, dtype=th.config.floatX))
        self.params.extend([self.W_ho, self.b])

        # define output layer for network
        self.output_layer = T.dot(self.hidden_layers[-1].output, self.W_ho) + self.b  # TODO parameterise output layer like for the other hidden layers

    def get_cost_updates(self, lr):
        """
        generic method for defining the update schedule and cost to use during network training
        :param lr: learning rate for gradient descent type methods
        :return: cost and updates for training step
        """
        # l2 regularisation
        l2 = T.sum(0)
        for p in self.params:
            l2 = l2 + (p ** 2).sum()

        # cost function parameterised by input received
        cost= self.loss(self.output_layer, self.targets) + (1e-3 *l2)

        # clipped vanilla gradient descent for parameter updates during network training
        gparams = [T.clip(g, -100, 100) for g in T.grad(cost, self.params)]
        updates = [(param, param - lr * gparam)
                    for param, gparam in zip(self.params, gparams)]

        return cost, updates


# TODO move the train network compilation as part of the class so less parameter passing needed in future

@func_timer
def train_network(model_name, train_inputs, train_targets, numbatches,
                  n_epochs, lr, n_in_input, n_out_input,
                  loss_input, hid_dim_input, hid_type_input):

    """
    method for instantiating and training a network using theano, providing all the configuration input for dynamically
    creating a network flavour using the network class.

    :param model_name: name of model used for naming output files, so should reflect configuration
    :param train_inputs: input data, as required and described by the network
    :param train_targets: target data, as required and described by the network. Strictly supervised setting.
    :param numbatches: num of batches to split the training for each training iteration
    :param n_epochs:  number of training iterations of gradient descent for the model object params
    :param lr: learnin rate for gradient descent
    # rest of input params described by the network class
    :return: trained network instance
    """

    # initialise symbolic theano variables for the autograd function compilation
    X = T.matrix('X')
    Y = T.matrix('Y')
    rng = np.random.RandomState(123)

    # instantiate network of the specified configuration. Pass inputs to network class
    network = Network(numpy_rng = rng,n_in = n_in_input,
                      n_out = n_out_input, loss = loss_input,
                      hid_dim = hid_dim_input, hid_type = hid_type_input,
                      inputs = X, targets = Y)

    # determine batch size given number of batches and length of data set
    batch_size = len(train_inputs)/numbatches

    # define cost and updates schedule given the network definition and proceed to compile theano function for training
    cost, updates = network.get_cost_updates(lr)

    # sets up computation graph
    train_model = th.function(inputs=[X, Y], outputs=cost, updates=updates)

    # train model batch wise for the complete number of iterations specified
    print '\n\n ######------------TRAINING MODEL %s, ' % hid_type_input

    indexes = len(train_inputs)
    train_errors = np.ndarray(n_epochs)

    for x in range(n_epochs):
        error = 0
        for index in range(numbatches):
            train_cost = train_model(train_inputs[index * batch_size:(index+ 1) * batch_size],
                                     train_targets[index * batch_size:(index+ 1) * batch_size])
            error += train_cost
            sys.stdout.write("\rEpoch %i, Batch %i, Error %f" % (x, index, train_cost))
            sys.stdout.flush()

        train_errors[x] = error

     # save model object and error history during training
    with open('Predictive model framework/Output/trained_models/' + model_name, 'wb') as g:
        pickle.dump(network, g)

    with open('Predictive model framework/Output/trained_models/' + model_name + '_errors', 'wb') as g:
        pickle.dump(train_errors, g)

    return network, train_errors


def test_network(network, test_inputs):

    """
    method for producing predictions given a trained model
    :param network: trained model object of class network
    :param test_inputs: input data to predict against
    :return:
    """
    # compile prediction function in theano
    predictions = th.function(inputs = [network.inputs], outputs = network.output_layer)

    # get input data in right shape
    numobvs = len(test_inputs)
    dim = len(test_inputs[0])
    test_inputs = np.reshape(test_inputs , (numobvs,dim))

    # collect prediction results
    preds=[]
    for i in test_inputs:
        i = np.reshape(i, (1,dim))
        preds.append(predictions(i))

    return preds








