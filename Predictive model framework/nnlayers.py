__author__ = 'Peter Hayes UCL CSML'

from utils import *
from mathfuncs import *
from tm import *
fx = th.config.floatX



class FeedForwardLayer(object):

    """
    Class for simple sigmoid hidden layer
    """

    def __init__(self, rng, input, n_in, n_out,
                 activation=th.tensor.nnet.sigmoid):

        self.input = input

        W = init_sig_param(rng, n_in, n_out)
        b_values = np.zeros((n_out,), dtype=fx)
        b = th.shared(value=b_values, name='b', borrow=True)

        self.W = W
        self.b = b
        self.params = [self.W, self.b]

        lin_output = T.dot(input, self.W) + self.b
        self.output = activation(lin_output)


class RNNLayer(object):
    """
    Class for simple recurrent neural network layer
    """
    def __init__(self, rng, input, n_in, n_out,
                 activation):

        self.input = input
        self.activation = activation

        # randomly initialise weights
        W_ih= simple_init(n_in, n_out) # TODO more sophisticated init schemes..
        W_hh= simple_init(n_out, n_out)

        self.W_ih = th.shared(name='W_ih', value= W_ih.astype(fx))
        self.W_hh = th.shared(name='W_hh', value=W_hh.astype(fx))
        self.bh  = th.shared(np.zeros(n_out, dtype=th.config.floatX))

        h0 = np.zeros(n_out, dtype=fx)
        self.h0 = th.shared(value=h0, name='h0')

        # recurrent mechanism used for looping through each time step and determining the hidden state
        def step(x_t, h_tm1):
            h_t = self.activation(T.dot(x_t, self.W_ih) + T.dot(h_tm1, self.W_hh)+ self.bh)
            return h_t

        # symbolic for loop for applying step at each time step
        self.h, _ = th.scan(step, sequences=self.input, outputs_info=self.h0)
        self.output = self.h
        self.params = [self.W_ih, self.W_hh, self.h0, self.bh]


class LSTMLayer(object):

    """
    Class for LSTM layer variant
    """

    def __init__(self, rng, input, n_in, n_out,
                 activation):

         # Randomly initialize the network parameters for the recurrent gating mechanism

        self.input = input
        self.activation = activation

        W_xi = simple_init(n_in, n_out)  # TODO more sophisticated init schemes..
        W_hi = simple_init(n_out, n_out)
        W_ci = simple_init(n_out, n_out) # TODO paramerterise number of memory cells
        W_xf = simple_init(n_in, n_out)
        W_hf = simple_init(n_out, n_out)
        W_cf = simple_init(n_out, n_out)
        W_xo = simple_init(n_in, n_out)
        W_ho = simple_init(n_out, n_out)
        W_co = simple_init(n_out, n_out)
        W_xc = simple_init(n_in, n_out)
        W_hc = simple_init(n_out, n_out)

        self.W_xi = th.shared(name='W_xi', value= W_xi)
        self.W_hi = th.shared(name='W_hi', value= W_hi)
        self.W_ci = th.shared(name='W_ci', value= W_ci)
        self.bi = th.shared(np.zeros(n_out, dtype=th.config.floatX))

        self.W_xf = th.shared(name='W_xf', value= W_xf)
        self.W_hf = th.shared(name='W_hf', value= W_hf)
        self.W_cf = th.shared(name='W_cf', value= W_cf)
        self.bf = th.shared(np.zeros(n_out, dtype=th.config.floatX))

        self.W_xo = th.shared(name='W_xo', value= W_xo)
        self.W_ho = th.shared(name='W_ho', value= W_ho)
        self.W_co = th.shared(name='W_co', value= W_co)
        self.bo = th.shared(np.zeros(n_out, dtype=th.config.floatX))

        self.W_xc = th.shared(name='W_xc', value= W_xc)
        self.W_hc = th.shared(name='W_hc', value= W_hc)
        self.bc = th.shared(np.zeros(n_out, dtype=th.config.floatX))

        # initialise the memory
        c0 = np.zeros(n_out, dtype=fx)
        self.c0 = th.shared(value=c0, name='c0')
        self.h0 = T.tanh(self.c0)

        # recurrent mechanism used for looping through each time step and determining the hidden state using explicit
        # memory in a cell controlled using a gating mechanism
        def step(x_t, h_tm1, c_tm1):

            i_t = activation(th.dot(x_t, self.W_xi) + th.dot(h_tm1, self.W_hi) + th.dot(c_tm1, self.W_ci) + self.bi)
            f_t = activation(th.dot(x_t, self.W_xf) + th.dot(h_tm1, self.W_hf) + th.dot(c_tm1, self.W_cf) +  self.bf)
            c_t = f_t * c_tm1 + i_t * self.activation(th.dot(x_t, self.W_xc) + th.dot(h_tm1, self.W_hc) +  self.bc)
            o_t = activation(th.dot(x_t, self.W_xo)+ th.dot(h_tm1, self.W_ho) + th.dot(c_t, self.W_co) + self.bo)
            h_t = o_t * T.tanh(c_t)

            return h_t, c_t

        # the hidden state `h` for the entire sequence, and the updated memory cell
        [self.h_t, self.c_t], _ = th.scan(step, sequences=self.input, outputs_info=[self.h0, self.c0]) # underscore means throw away updates returned from scan

        self.output = self.h_t

        self.params = [ self.c0,
                        self.W_xi, self.W_hi,self.W_ci, self.bi,
                        self.W_xf, self.W_hf, self.W_cf, self.bf,
                        self.W_xo, self.W_ho, self.W_co, self.bo,
                        self.W_xc, self.W_hc, self.bc]



class LIFSpikeLayer(object):

    """
    Implementation of leaky integrate and fire spiking neuron model, as specified in:
    David Barber; Bayesian Reasoning and Machine Learning section 26.5.4
    Implemented in the context of this project, as this mechanism shows promise theoretically for dealing
    with temporal patterns with its refactory dynamics. However we ran out of time for including in the theory and
    experimentation
    """

    def __init__(self, rng, input, n_in, n_out,
                 activation):

        # Randomly initialize the network parameters

        self.input = input
        self.activation = activation

        W = simple_init(n_in, n_out) # TODO more sophisticated init schemes..
        fired = 1
        rest = 1
        al = 1

        self.W = th.shared(name='W', value= W)
        self.al = th.shared(name='alpha', value= al)
        self.fired = th.shared(name='reset_fired', value= fired)
        self.rest = th.shared(name='reset_rest', value= rest)

        v0 = np.zeros(n_out, dtype = fx)
        self.v0 = th.shared(value = v0, name = 'v0')

        a0 = np.zeros(n_out, dtype = fx)
        self.a0 = th.shared(value =a0, name = 'a0')

        # Recurrent pattern that replicates potential changing in neuron followed by refactory period
        def step(x_t, a_tm1, v_tm1):

            a_t = (self.al*(a_tm1) + T.dot(x_t,self.W)
                + self.rest*(1-self.al)) * (1-v_tm1) + (v_tm1*self.fired)

            v_t = self.activation(a_t)

            return a_t, v_t

        [self.a_t, self.v_t], _ = th.scan(step, sequences=self.input, outputs_info=[self.a0, self.v0])

        self.output = self.v_t

        self.params = [self.W, self.v0, self.a0]


class NTM_RecurrentControllerLayer(object):

    """
    Class for Neural Turing machine recurrent neural network controller.
    """

    def __init__(self, rng, input, n_in, n_out,
                 activation):

        self.memory_size = (100, 10) # TODO make this an input parameter
        self.shift_width = 3
        self.shift_conv = scipy.linalg.circulant(np.arange(self.memory_size[0])).T[np.arange(
            -(self.shift_width // 2),
            (self.shift_width // 2) + 1)][::-1]

        self.input = input
        self.activation = activation

        # controller mechanism params
        self.W_ih = th.shared(name='W_ih', value= simple_init(n_in, n_out).astype(fx)) # TODO more sophisticated init schemes..
        self.W_hh = th.shared(name='W_hh', value=simple_init(n_out, n_out).astype(fx))
        self.W_rh = th.shared(name='W_rh', value=simple_init(self. memory_size[1], n_out).astype(fx))
        self.b_ih = th.shared(np.zeros(n_out, dtype=th.config.floatX))

        self.W_erase = th.shared(name='W_erase', value=  simple_init(n_in, self. memory_size[1]).astype(fx))
        self.b_erase = th.shared(np.zeros(self. memory_size[1], dtype=th.config.floatX))
        self.W_add = th.shared(name='W_add', value=  simple_init(n_in, self. memory_size[1]).astype(fx))
        self.b_add = th.shared(np.zeros(self.memory_size[1], dtype=th.config.floatX))

        # read head mechanism params
        self.W_key_read = th.shared(name='W_key_read ', value=  simple_init(n_in, self. memory_size[1]).astype(fx))
        self.b_k_read = th.shared(np.zeros(self. memory_size[1], dtype=th.config.floatX))
        self.W_beta_read = th.shared(name='W_beta_read ', value= np.zeros(n_in).astype(fx))
        self.b_beta_read = th.shared(np.zeros(1, dtype=th.config.floatX))
        self.W_gate_read = th.shared(name='W_gate_read', value= simple_init(n_in,0).astype(fx))
        self.b_gate_read = th.shared(np.zeros(1, dtype=th.config.floatX))
        self.W_shift_read = th.shared(name='W_shift_read', value= simple_init(n_in, self.shift_width).astype(fx))
        self.b_shift_read  = th.shared(np.zeros(self.shift_width, dtype=th.config.floatX))
        self.W_gamma_read = th.shared(name='W_gamma_read', value= simple_init(n_in,0).astype(fx))
        self.b_gamma_read = th.shared(np.zeros(1, dtype=th.config.floatX))

        # write head mechanism params
        self.W_key_write = th.shared(name='W_key_write ', value=  simple_init(n_in, self. memory_size[1]).astype(fx))
        self.b_k_write = th.shared(np.zeros(self. memory_size[1], dtype=th.config.floatX))
        self.W_beta_write = th.shared(name='W_beta_write ', value= np.zeros(n_in).astype(fx))
        self.b_beta_write = th.shared(np.zeros(1, dtype=th.config.floatX))
        self.W_gate_write = th.shared(name='W_gate_write', value= simple_init(n_in,0).astype(fx))
        self.b_gate_write = th.shared(np.zeros(1, dtype=th.config.floatX))
        self.W_shift_write = th.shared(name='W_shift_write', value= simple_init(n_in, self.shift_width).astype(fx))
        self.b_shift_write  = th.shared(np.zeros(self.shift_width, dtype=th.config.floatX))
        self.W_gamma_write = th.shared(name='W_gamma_write', value= simple_init(n_in,0).astype(fx))
        self.b_gamma_write = th.shared(np.zeros(1, dtype=th.config.floatX))

        # initial memory and states
        h0 = np.zeros(n_out, dtype=fx)
        self.h0 = th.shared(value=h0, name='h0')

        m0 = 2 * (np.random.rand(self.memory_size[0], self.memory_size[1]) - 0.5) # initialise memory
        self.m0 = th.shared(value=m0, name='m0')

        W_rm_0 =  simple_init(self. memory_size[0],0)
        W_wm_0 =  simple_init(self. memory_size[0],0)

        self.W_rm_0 = th.shared(value=W_rm_0, name='W_rm_0')
        self.W_wm_0 = th.shared(value=W_wm_0, name='W_wm_0')

        # recurrent controller mechanism and memory interaction
        def run_turing_machine(x_t, m_tm1, h_tm1, W_rm_tm1, W_wm_tm1): # TODO incorporate bias terms

            # control mechanism
            r_t, m_t = machine_memoryOps(self, x_t, m_tm1, params = [W_wm_tm1, W_rm_tm1])
            h_t = activation(T.dot(x_t, self.W_ih)+ T.dot(r_t, self.W_rh) + T.dot(h_tm1, self.W_hh)) #+ self.b_ih)

            # read head addressing mechanism
            W_rm_t = machine_addressing(self, x_t, m_t, W_rm_tm1, mode = 'read')

            # write head addressing mechanism
            W_wm_t = machine_addressing(self, x_t, m_t, W_wm_tm1, mode = 'write')

            return m_t, h_t, W_rm_t, W_wm_t

        [self.m, self.h, self.W_rm, self.W_wm], _ = th.scan(run_turing_machine, sequences=self.input,
                                                        outputs_info=[self.m0, self.h0, self.W_rm_0, self.W_wm_0])


        self.output = self.h
        self.params = [self.h0, self.m0,

                       self.W_ih, self.W_hh, self.W_rh,# self.b_ih, ADD REST OF BIAS BACK

                       self.W_key_read, self.W_beta_read, self.W_gate_read,
                       self.W_shift_read, self.W_gamma_read,

                       self.W_key_write, self.W_beta_write, self.W_gate_write,
                       self.W_shift_write,  self.W_gamma_write]


