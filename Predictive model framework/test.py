__author__ = 'Peter Hayes UCL CSML'

from network import test_network
from utils import *
import itertools as it

"""
EXECUTABLE SCRIPT FOR AUTOMATICALLY TESTING MODELS AND PRODUCING OUTPUT ACROSS A COMBINATION OF TEST CASES
"""

def gaussian_like(predictions, targets, thresholds):
    """
    Using ZScore compute whether prediction is anomalous or not
    :param predictions: predicted output from model
    :param targets: actual data
    :param thresholds: Zscore threshold to test anomaly
    :return: vector of zero and ones, ones where prediction deemed to be anomalous
    """
    residuals = predictions - targets
    mean = np.mean(residuals)
    std = np.std(residuals)
    zscore = (residuals - mean)/std
    results = [1 if abs(np.mean(i)) > thresholds else 0 for i in zscore]

    return results


if __name__ == '__main__':

    """
    Executable for testing models
    """
    # define input params

    # set which model variants are to be tested
    models = ['_stacked2_LSTM', '_stacked2_RNN',
              '_vanilla_LSTM', '_vanilla_RNN']

    # set which model variants are to be tested
    data_name = ['ECG', 'SHUTTLE', 'YAHOO', 'POWER']

    # define which thresholds should be tested for zscore on anomaly detection
    thresholds = [3, 5, 7]

    # get all test input combinations for automatically processing
    combs = list(it.product(data_name, models, thresholds))
    target_names = ["anomaly","not_anomaly"]
    pred_anoms = {}
    class_results = {}

    filename = 'Predictive model framework/Output/anomaly_stats/all_stats.csv'
    results_csv = open(filename, 'w')

    # cycle through each combination, generate load data, load model, generate predictions and test for anomaly detection
    # and append result to results file

    for c in combs:

        # get test data
        data_path = 'data/'+ c[0] +'/test/'
        counter = 0
        for fname in os.listdir(data_path):
            if not (fname.startswith('.')):
                data = np.genfromtxt(data_path + fname, delimiter=',')
                if counter == 0:
                    test_data = data
                    counter +=1
                else:
                    test_data = np.append(test_data, data, axis = 0)

        # separate anomaly lables
        test_anoms = test_data[:,-1]
        test_data = test_data[:,:-1]

        # load trained model
        with open('Predictive model framework/Output/trained_models/'+c[0]+c[1]) as f:
            trained_model = pickle.load(f)

        # get predictions using the predict function for the model instance
        test_preds =  test_network(trained_model, test_data)

        # store predictions
        filename = 'Predictive model framework/Output/test_predictions/'+c[0]+c[1] +'.csv'
        preds_csv = open(filename, 'w')
        for i in test_preds:
            preds_csv.write('%s\n' % i)
        preds_csv.close()

        # for a given threshold value determine what is considered an anomaly under the model
        pred_anoms[c] = gaussian_like(test_preds, test_data, c[2])

        # get classification stats
        res = classification_report(test_anoms, pred_anoms[c],target_names=target_names, labels= (1, 0))
        class_results[c] = res.split("\n")

         # store stats
        results_csv.write('\n%s,%i\n' % (c[0:2], c[2]))
        for i in class_results[c] :
            results_csv.write('%s\n' % i)

        # print progress
        print c
        print res

    print '\nFile %s created for inspection' % filename
    results_csv.close()










