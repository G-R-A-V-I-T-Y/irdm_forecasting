__author__ = 'Peter Hayes UCL CSML'

from mathfuncs import *

"""
MODULE CONTAINING METHODS FOR MEMORY INTERACTION USED BY THE NEURAL TURING MACHINE MECHANISM
"""

def machine_addressing(self, x_t, m_t, W_tm1, mode = 'read'):

    """
    :param x_t: input data sequence at time t
    :param m_t: memory at time t
    :param W_tm1: head weight at previous time step
    :param mode: whether the addressing is for read or write
    :return:the weights for use in the machine_memoryOps() function for updating and reading from memory at time t
    """

    # content addressing
    key_t = T.nnet.sigmoid(T.dot(x_t, eval('self.W_key_' + mode))) #+ eval('self.b_k_' + mode) # TODO incorporate bias terms
    beta_t = T.nnet.softplus(T.dot(x_t, eval('self.W_beta_' + mode))) #+ eval('self.b_beta_' + mode))
    sim = beta_t * cosine_similarity(key_t , m_t)
    W_content = vector_softmax(sim)

    # location addressing
    gate_t = T.nnet.sigmoid(T.dot(x_t, eval('self.W_gate_' + mode))) #+ eval('self.b_gate_' + mode))
    W_rm_t= gate_t* W_content + (1-gate_t) * W_tm1 # gating

    shift_t = vector_softmax(T.dot(x_t, eval('self.W_shift_' + mode))) #+ eval('self.b_shift_' + mode) )
    W_rm_t=  shift_convolve(W_rm_t, shift_t, self.shift_conv)# circular convolution

    gamma_t = T.nnet.softplus(T.dot(x_t, eval('self.W_gamma_' + mode))) #+ eval('self.b_gamma_' + mode))
    W_t = W_rm_t ** gamma_t/T.sum( W_rm_t ** gamma_t) # sharpening

    # TODO use more robust non linear activation, to help aid training problem of exploring gradient
    return W_t

def machine_memoryOps(self, x_t, m_tm1, params = []):
    """

    :param x_t: input data sequence at time t
    :param m_tm1: memory at time t-1 to be read from and updated
    :param params: reading and writing weights
    :return:read vector and updated memory at time t
    """
    # set current memory
    m_t = m_tm1

    # generate add and erase vectors
    e_t = T.nnet.sigmoid(T.dot(x_t, self.W_erase) )
    a_t = T.nnet.sigmoid(T.dot(x_t, self.W_add))

    # update memory
    m_erase = m_t * (1 - (T.outer(params[0],e_t)))
    m_t = m_erase + T.outer(params[0], a_t)

    # read data from memory
    r_t = T.dot(params[1],m_t)

    return r_t, m_t