__author__ = 'Peter Hayes UCL CSML'

from network import *
from mathfuncs import *
from nnlayers import *

"""
EXECUTABLE SCRIPT FOR INSTANTIATING AND TRAINING NETWORK MODELS
"""

# Step 1: Specify inputs:
dataset = 'YAHOO' # name of data set folder which should contain train and test folders
data_path = 'data/'+dataset+'/train/'
prediction_window = 2 # lag to poduce for the training target values
n = 500 # training epochs

# Step 2: Ingest data
counter = 0
for fname in os.listdir(data_path):
    if not (fname.startswith('.')):
        data = np.genfromtxt(data_path + fname, delimiter=',') # for power, must pop last column
        data = running_view(data , prediction_window)
        if counter == 0:
            train_data = data
            counter +=1
        else:
            train_data = np.append(train_data, data, axis = 0)

train_data = np.array(train_data)
# Specify input and output data vectors change appropriately for MV data.
numobvs = len(train_data)
inputs = np.reshape(train_data[:,0], (numobvs,1))
outputs = np.reshape(train_data[:,1], (numobvs,1))


# Step 3: configure and train network variants for the data set

# train vanilla LSTM
trainedLSTM_network, trainLSTM_errors = train_network(model_name = dataset+'_vanilla_LSTM', train_inputs = inputs, train_targets = outputs, numbatches =20,
                               n_epochs = n, lr = 0.01,
                                n_in_input = 1, n_out_input = 1, loss_input = mse,
                                hid_dim_input=[30], hid_type_input=[LSTMLayer])

# train vanilla RNN
trainedRNN_network, trainRNN_errors = train_network(model_name = dataset+'_vanilla_RNN', train_inputs = inputs, train_targets = outputs, numbatches =20,
                               n_epochs = n, lr = 0.01,
                                n_in_input = 1, n_out_input = 2, loss_input = mse,
                                hid_dim_input=[30], hid_type_input=[RNNLayer])

# train stacked LSTM
trainedSLSTM_network, trainSLSTM_errors = train_network(model_name = dataset+'_stacked2_LSTM',train_inputs = inputs, train_targets = outputs, numbatches =20,
                               n_epochs = n, lr = 0.01,
                                n_in_input = 1, n_out_input = 1, loss_input = mse,
                                hid_dim_input=[30, 30], hid_type_input=[LSTMLayer, LSTMLayer])

# train stacked RNN
trainedSRNN_network, trainSRNN_errors = train_network(model_name = dataset+'_stacked2_RNN', train_inputs = inputs, train_targets = outputs, numbatches =20,
                               n_epochs = n, lr = 0.01,
                                n_in_input = 1, n_out_input = 1, loss_input = mse,
                                hid_dim_input=[30, 30], hid_type_input=[RNNLayer, RNNLayer])

# plot resulting error Vs training epochs
plt.plot(np.arange(n), trainLSTM_errors,'r-',label = 'LSTM')
plt.plot(np.arange(n), trainRNN_errors,'g-',label = 'RNN')
plt.plot(np.arange(n), trainSLSTM_errors ,'b-',label = 'SLSTM')
plt.plot(np.arange(n), trainSRNN_errors  ,'k-',label = 'SRNN')
plt.xlabel('epochs')
plt.ylabel('error')
plt.legend()
plt.show()


