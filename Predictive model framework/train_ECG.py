__author__ = 'Peter Hayes UCL CSML'

from network import *
from mathfuncs import *
from nnlayers import *


"""
SAME AS TRAIN.PY:
EXECUTABLE SCRIPT FOR INSTANTIATING AND TRAINING NETWORK MODELS WITH ECG DATA - MULTIVARIATE AND SO DATA INGEST STEP
WORKS SLIGHTLY DIFFERENT - TO DO GENERALISE PRE PROCESSING OF DATA AND PREDICTION WINDOW FOR ANY DIM INPUT
"""

data_path = 'data/ECG/train/'

for fname in os.listdir(data_path):
    if not (fname.startswith('.')):
            data = np.genfromtxt(data_path + fname,
                           delimiter=',')
            #data = running_view(data , 2)
            data = np.concatenate([data[0:-1,:],data[1:,:]], axis=1)
            train_data = data  #np.append(train_data, data, axis = 0)

numobvs = len(train_data)-1
inputs = np.reshape(train_data[1:,0:2], (numobvs,2))
outputs = np.reshape(train_data[1:,2:], (numobvs,2))

n= 500


trainedLSTM_network, trainLSTM_errors = train_network(model_name = 'ECG_vanilla_LSTM', train_inputs = inputs, train_targets = outputs, numbatches =20,
                               n_epochs = n, lr = 0.01,
                                n_in_input = 2, n_out_input = 2, loss_input = mse,
                                hid_dim_input=[30], hid_type_input=[LSTMLayer])

trainedRNN_network, trainRNN_errors = train_network(model_name = 'ECG_vanilla_RNN', train_inputs = inputs, train_targets = outputs, numbatches =20,
                               n_epochs = n, lr = 0.01,
                                n_in_input = 2, n_out_input = 2, loss_input = mse,
                                hid_dim_input=[30], hid_type_input=[RNNLayer])

trainedSLSTM_network, trainSLSTM_errors = train_network(model_name = 'ECG_stacked2_LSTM',train_inputs = inputs, train_targets = outputs, numbatches =20,
                               n_epochs = n, lr = 0.01,
                                n_in_input = 2, n_out_input = 2, loss_input = mse,
                                hid_dim_input=[30, 30], hid_type_input=[LSTMLayer, LSTMLayer])

trainedSRNN_network, trainSRNN_errors = train_network(model_name = 'ECG_stacked2_RNN', train_inputs = inputs, train_targets = outputs, numbatches =20,
                               n_epochs = n, lr = 0.01,
                                n_in_input = 2, n_out_input = 2, loss_input = mse,
                                hid_dim_input=[30, 30], hid_type_input=[RNNLayer, RNNLayer])

plt.plot(np.arange(n), trainLSTM_errors,'r-',label = 'LSTM')
plt.plot(np.arange(n), trainRNN_errors,'g-',label = 'RNN')
plt.plot(np.arange(n), trainSLSTM_errors ,'b-',label = 'SLSTM')
plt.plot(np.arange(n), trainSRNN_errors  ,'k-',label = 'SRNN')
plt.xlabel('epochs')
plt.ylabel('error')
plt.legend()
plt.show()


