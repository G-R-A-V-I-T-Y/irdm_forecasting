__author__ = 'Peter Hayes UCL CSML'

__author__ = 'Peter Hayes UCL CSML'

from network import *
from mathfuncs import *
from nnlayers import *

"""
SAME AS TRAIN.PY
EXECUTABLE SCRIPT FOR INSTANTIATING AND TRAINING NETWORK MODELS WITH AN NTM LAYER!
"""

# Step 1: Specify inputs:
dataset = 'YAHOO' # name of data set folder which should contain train and test folders
data_path = 'data/'+dataset+'/train/'
prediction_window = 2 # lag to poduce for the training target values
n = 100 # training epochs

# Step 2: Ingest data
counter = 0
for fname in os.listdir(data_path):
    if not (fname.startswith('.')):
        data = np.genfromtxt(data_path + fname, delimiter=',') # for power, must pop last column
        data = running_view(data , prediction_window)
        if counter == 0:
            train_data = data
            counter +=1
        else:
            train_data = np.append(train_data, data, axis = 0)

train_data = np.array(train_data)
# Specify input and output data vectors change appropriately for MV data.
numobvs = 50 # len(train_data) # toy example of 50 data points only
inputs = np.reshape(train_data[0:50,0], (numobvs,1))
outputs = np.reshape(train_data[0:50,1], (numobvs,1))


# Step 3: configure and train network variants for the data set

# train vanilla NTM layered network
trainedNTM_network, trainNTM_errors = train_network(model_name = dataset+'_vanilla_LSTM', train_inputs = inputs, train_targets = outputs, numbatches =1,
                               n_epochs = n, lr = 0.01,
                                n_in_input = 1, n_out_input = 1, loss_input = mse,
                                hid_dim_input=[30], hid_type_input=[NTM_RecurrentControllerLayer])


plt.plot(np.arange(n), trainNTM_errors,'r-',label = 'NTM')
plt.xlabel('epochs')
plt.ylabel('error')
plt.legend()
plt.show()


