__author__ = 'Peter Hayes UCL CSML'

import theano as th
import sys
import time
import numpy as np
import matplotlib.pyplot as plt
import os
import theano.tensor as T
import scipy
from sklearn.metrics import classification_report
import pickle
fx = th.config.floatX

"""
Module containing useful utility functions, and import statements required across the solution
"""

def init_sig_param(rng, n_in, n_out):

    W_values = np.asarray(rng.uniform(
                    low=-np.sqrt(6. / (n_in + n_out)),
                    high=np.sqrt(6. / (n_in + n_out)),
                    size=(n_in, n_out)),dtype=th.config.floatX)

    W_values *= 4
    W = th.shared(value=W_values, name='W', borrow=True)

    return W

def simple_init(n_in, n_out):
    """
    simple random weight initialisation
    """
    if n_out ==0:
        W = np.asarray(np.random.uniform(size=n_in,low=-.01, high=.01), dtype=fx)
    else:
        W = np.asarray(np.random.uniform(size=(n_in, n_out),low=-.01, high=.01), dtype=fx)

    return W


def runEpochs(n_epochs, numbatches, train_fn, model_class):
    '''_
        runs through training epochs for a given theano function
    '''
    print '######------------TRAINING MODEL %s, ' % model_class
    for epoch in xrange(n_epochs):
        for nindex in range(numbatches):
            c = train_fn(nindex) #compute cost

            sys.stdout.write("\rEpoch %i, Batch id %i, Error %f" % (epoch, nindex, c))
            sys.stdout.flush()

def running_view(array, window, axis=-1):
    """
    ref: http://stackoverflow.com/questions/21229503/creating-an-numpy-matrix-with-a-lag
    """
    shape = list(array.shape)
    shape[axis] -= (window-1)
    assert(shape[axis]>0)
    return np.lib.index_tricks.as_strided(array, shape + [window], array.strides + (array.strides[axis],))


# ------------------------------ LOGGING AND PROFILING FUNCTIONS--------------------
def func_timer(func):
    """Times how long the function took."""

    def f(*args, **kwargs):
        start = time.time()
        results = func(*args, **kwargs)
        print "\nElapsed: %.2fs" % (time.time() - start)
        return results

    return f

